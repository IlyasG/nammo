function getAppTitle(){
	return {
		country:'Greece',
		town:'Sparta',
		movie:'300',
		locateTown:function(){
			return this.town + ' is in ' + this.country;
		}
	};
}

exports.getAppTitle = getAppTitle;
