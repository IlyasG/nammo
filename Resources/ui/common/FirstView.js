//FirstView Component Constructor
function FirstView() {
	var Cloud = require('ti.cloud');
	Cloud.debug = true;  // optional; if you add this line, set it to false for production
	//alert("Cloud set");
	//create object instance, a parasitic subclass of Observable
	var self = Ti.UI.createView();
	var ex = require('ui/common/getAppTitle');
	var theResult = ex.getAppTitle();
	
	var field1 = Ti.UI.createTextField({
		top:10,
		left:10
	});
	self.add(field1);
	var field2 = Ti.UI.createTextField({
		top:30,
		left:10
	});
	self.add(field2);
	var field3 = Ti.UI.createTextField({
		top:50,
		left:10
	});
	self.add(field3);
	var field4 = Ti.UI.createTextField({
		top:70,
		left:10
	});
	self.add(field4);
	var field5 = Ti.UI.createTextField({
		top:90,
		left:10
	});
	self.add(field5);
	var field6 = Ti.UI.createTextField({
		top:110,
		left:10
	});
	self.add(field6);
	
	//label using localization-ready strings from <app dir>/i18n/en/strings.xml
	var label = Ti.UI.createLabel({
		color:'#000000',
		text:String.format(L('welcome'), theResult.town),
		height:'auto',
		width:'auto'
	});
	self.add(label);
	
	var testButton = Ti.UI.createButton({
		title: 'Create Account',
		height:'auto',
		width:'auto',
		left:50,
		top:50
	});
	self.add(testButton);
	
	//Add behavior for UI
	label.addEventListener('click', function(e) {
		if(prompt("Enter your color") == "Red")
			label.color = '#FF0000';
		else{
			alert("Switching to default color");
			label.color = '#0000FF';
		}
		var label2 = Ti.UI.createLabel({
			color: '#FFAA00',
			text: theResult.locateTown(),
			height:'auto',
			width:'auto',
			left:30,
			top:10
		})
		self.add(label2);
	});
	
	testButton.addEventListener('click', function(e){
		Cloud.Users.create({
			username: field1.value,
		    email: field2.value,
		    password: field3.value,
		    password_confirmation: field4.value,
		    first_name: field5.value,
		    last_name: field6.value
		}, function (e) {
		    if (e.success) {
				alert("SUCCESS!!! SPARTA WINS!!!");
		    } else {
		        alert('Error: SPARTA LOST');
		    }
		});
	});
/*	testButton.addEventListener('click', function(e){
		Cloud.Users.create({
		    username: prompt("Enter your Username"),
		    password: prompt("Enter your Password"),
		    password_confirmation: prompt("Confirm your Password"),
		    first_name: prompt("Enter your First Name"),
		    last_name: prompt("Enter your Last Name")
		}, function (e) {
		    if (e.success) {
				alert("SUCCESS!!! SPARTA WINS!!!");
		    } else {
		        alert("DUMMASS! SPARTA LOST!");
		    }
		});
	});*/
	
	return self;
}

module.exports = FirstView;
